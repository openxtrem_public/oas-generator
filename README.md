<h1>OAS-Generator</h1>

<h2>About</h2>

Convert Symfony routes collection to Open Api Specifications.
OAS validation with cebe/php-openapi

<br>How to use :
```
$oas = new Specifications();
$oas->setVersion('1.0.0')
    ->setTitle('OX APIs documentation')
    ->setDescription('This documentation ...')
    ->setContact('admin', 'admin@info.fr')
    ->setLicense('GPL', 'https://openxtrem.com/licenses/gpl.html')
    ->addTag('system', 'This is a tag');
    ->addServer('http://lorem-ipsum.fr', 'description')
    ->addSecurity(
        'Token', 'apiKey', 'header token', [
                   'in'   => 'header',
                   'name' => 'X-OXAPI-KEY',
               ])
    ->addSchema(new Scheam('lorem', 'object'));

$routes = new RouteCollection(); // your routes

$generator     = new Generator($oas, $routes);
$documentation = $generator->generate();
```


<h2>How to tests ?</h2>

```
 vendor/bin/phpunit -c phpunit.xml
 ```

<h2>Need help ?</h2>

```
https://www.openapis.org/
 ```
