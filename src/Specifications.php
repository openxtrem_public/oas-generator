<?php
/**
 * @package Mediboard\Core\OpenApi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator;

use Ox\Components\OASGenerator\Schema\Schema;

/**
 * @see https://swagger.io/docs/specification
 */
class Specifications
{
    public const  CURRENT_OAS_VERSION           = "3.0.1";
    private const ALLOWED_SECURITY_FIXED_FIELDS = [
        "type",
        "description",
        "name",
        "in",
        "scheme",
        "bearerFormat",
        "flows",
        "openIdConnectUrl",
    ];

    private string $version;
    private string $title;
    private string $description;

    private array $contact = [];

    private array $license = [];

    private $servers = [];

    private $paths = [];

    private $tags = [];

    private $security = [];

    private $components = [];

    private $responses = [];


    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @param string $title
     *
     * @return Specifications
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $description
     *
     * @return Specifications
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param string $name
     * @param string $email
     *
     * @return Specifications
     */
    public function setContact(string $name, string $email): self
    {
        $this->contact = [
            'name'  => $name,
            'email' => $email,
        ];

        return $this;
    }

    /**
     * @param string $name
     * @param string $url
     *
     * @return Specifications
     */
    public function setLicense(string $name, string $url): self
    {
        $this->license = [
            'name' => $name,
            'url'  => $url,
        ];

        return $this;
    }

    /**
     * An array of Server Objects, which provide connectivity information to a target server
     *
     * @param string $url
     * @param string $description
     *
     * @return Specifications
     */
    public function addServer(string $url, string $description): self
    {
        $this->servers[] = [
            'url'         => $url,
            'description' => $description,
        ];

        return $this;
    }

    /**
     * The Schema Object allows the definition of input and output data types.
     * These types can be objects, but also primitives and arrays.
     * This object is a superset of the JSON Schema Specification Draft 2020-12.
     *
     */
    public function addSchema(Schema $schema): self
    {
        $this->components["schemas"][$schema->getName()] = $schema->toArray();

        return $this;
    }

    public function addSchemaReference(string $schema_name, string $ref): self
    {
        $this->components["schemas"][$schema_name] = ["\$ref" => $ref];
        return $this;
    }

    /**
     * A list of tags used by the specification with additional metadata
     *
     * @param string $name
     * @param string $description
     *
     * @return $this
     */
    public function addTag(string $name, string $description): self
    {
        $this->tags[] = [
            'name'        => $name,
            'description' => $description,
        ];

        return $this;
    }

    /**
     * Each name MUST correspond to a security scheme which is declared in the Security Schemes under the Components
     * @see https://swagger.io/specification/#security-scheme-object
     * Object
     *
     * @param string      $name
     * @param string      $type
     * @param string|null $description
     * @param array       $fixed_field
     *
     * @return Specifications
     * @throws OASException
     */
    public function addSecurity(string $name, string $type, string $description = null, array $fixed_field = []): self
    {
        // If the security scheme is of type "oauth2" or "openIdConnect",
        // then the value is a list of scope names required for the execution, and the list MAY be empty
        // if authorization does not require a specified scope.
        // For other security scheme types, the array MUST be empty.
        $this->security[] = [$name => []];

        $schema         = [];
        $schema['type'] = $type;
        if ($description !== null) {
            $schema['description'] = $description;
        }

        if (!empty($fixed_field)) {
            foreach ($fixed_field as $field => $value) {
                if (!in_array($field, self::ALLOWED_SECURITY_FIXED_FIELDS)) {
                    throw new OASException('Invalid security fixed fields ' . $field);
                }
                $schema[$field] = $value;
            }
        }

        $this->components["securitySchemes"][$name] = $schema;

        return $this;
    }

    public function convert(): array
    {
        $oas               = [];
        $oas['openapi']    = static::CURRENT_OAS_VERSION;
        $oas['info']       = [
            'title'       => $this->title,
            'description' => $this->description,
            'version'     => $this->version,
            'contact'     => $this->contact,
            'license'     => $this->license,
        ];
        $oas['tags']       = $this->tags;
        $oas['servers']    = $this->servers;
        $oas['security']   = $this->security;
        $oas['components'] = $this->components;
        $oas['paths']      = [];

        return $oas;
    }

}
