<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator;


use cebe\openapi\exceptions\TypeErrorException;
use cebe\openapi\Reader;
use Ox\Components\OASGenerator\Attributes\BodyAttribute;
use Ox\Components\OASGenerator\Attributes\ParameterAttribute;
use Ox\Components\OASGenerator\Attributes\ResponseAttribute;
use Ox\Components\OASGenerator\Enum\ContentType;
use Ox\Components\OASGenerator\Enum\Type;
use ReflectionException;
use ReflectionMethod;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Yaml;

/**
 * Generate OAS from routes collection
 */
class Generator
{
    const RETURN_TYPE_YML  = 'yml';
    const RETURN_TYPE_JSON = 'json';

    private array $documentation = [];

    private Specifications  $specifications;
    private RouteCollection $route_collection;


    public function __construct(Specifications $specifications, RouteCollection $route_collection)
    {
        $this->specifications   = $specifications;
        $this->route_collection = $route_collection;
    }


    /**
     *
     * @param string|null $return_type
     *
     * @return string|array
     * @throws OASException
     * @throws TypeErrorException
     */
    public function generate(string $return_type = null)
    {
        // Specifications
        $this->documentation = $this->specifications->convert();

        // Paths from RouteCollection
        foreach ($this->route_collection as $route_name => $route) {
            $this->addPath($route_name, $route);
        }

        // Validate OAS
        $doc_json = json_encode($this->documentation);
        $openapi  = Reader::readFromJson($doc_json);
        if (!$openapi->validate()) {
            throw new OASException(implode('-', $openapi->getErrors()));
        }

        if ($return_type === self::RETURN_TYPE_YML) {
            return Yaml::dump($this->documentation);
        }

        if ($return_type === self::RETURN_TYPE_JSON) {
            return $doc_json;
        }

        return $this->documentation;
    }

    /**
     *
     * @param string $route_name
     * @param Route  $route
     *
     * @return void
     * @throws ReflectionException
     */
    private function addPath(string $route_name, Route $route): void
    {
        // no doc ?
        if ($route->getOption('openapi') === false) {
            return;
        }

        if ($route->getDefault('_controller')) {
            [$controller, $method] = explode('::', $route->getDefault('_controller'));
            $reflection_method = new ReflectionMethod($controller, $method);
        } else {
            $reflection_method = null;
        }


        // auto-generate !
        $oas = [];

        // Summary
        if ($route->hasOption('summary')) {
            $oas['summary'] = $route->getOption('summary');
        }

        // Description
        if ($route->hasOption('description')) {
            $oas['description'] = $route->getOption('description');
        }

        // tag (route prefix)
        $oas['tags'] = (array)explode('_', $route_name)[0];

        // parameters
        $oas['parameters'] = [];

        // query params
        if ($parameters = $route->getOption('parameters')) {
            foreach ($parameters as $param_name => $type_route) {
                $type_oas = $this->getParametersTypeOAS($type_route);
                $param    = [
                    'in'       => 'query',
                    'name'     => $param_name,
                    'schema'   => [
                        'type' => $type_oas ?? 'string',
                    ],
                    'required' => false,
                ];

                if ($type_oas === null && is_string($type_route)) {
                    if (preg_match('/[a-zA-Z_\-0-9@]+\|[a-zA-Z_|\-0-9@]+/', $type_route) !== 0) {
                        $param['schema']['enum'] = explode('|', $type_route);
                    } elseif (@preg_match("#$type_route#", '') !== false) {
                        /* No Exception is thrown here because the regexp is never actually executed (outside of Swagger) */
                        $param['schema']['pattern'] = $type_route;
                    }
                }

                $oas['parameters'][] = $param;
            }
        } elseif ($reflection_method) {
            foreach ($reflection_method->getAttributes(ParameterAttribute::class) as $reflection_attribute) {
                /** @var ParameterAttribute $attribute */
                $attribute           = $reflection_attribute->newInstance();
                $oas['parameters'][] = [
                    'in'          => $attribute->getIn()->value,
                    'name'        => $attribute->getName(),
                    'schema'      => [
                        'type' => $attribute->getType()->value,
                    ],
                    'description' => $attribute->getDescription(),
                    'required'    => $attribute->isRequired(),
                ];
            }
        }

        // path params
        if ($requirements = $route->getRequirements()) {
            foreach ($requirements as $param_name => $type_route) {
                $type_oas = $this->getParametersTypeOAS($type_route);

                $param = [
                    'in'       => 'path',
                    'name'     => $param_name,
                    'schema'   => [
                        'type' => $type_oas ?? 'string',
                    ],
                    'required' => true,
                ];

                if ($type_oas === null && is_string($type_route)) {
                    if (preg_match('/[a-zA-Z_\-0-9@]+\|[a-zA-Z_|\-0-9@]+/', $type_route) !== 0) {
                        $param['schema']['enum'] = explode('|', $type_route);
                    } else {
                        /* Throws an ErrorException in case of an invalid regexp in the requirement */
                        set_error_handler(function ($type, $message) use ($route_name, $param_name, $type_route) {
                            if (str_contains($message, 'preg_match():')) {
                                $message = "$route_name: requirement $param_name : Invalid regexp \"$type_route\" :"
                                    . str_replace('preg_match():', '', $message);
                            }
                            throw new \ErrorException($message);
                        });
                        try {
                            preg_match("#$type_route#", '');
                            $param['schema']['pattern'] = $type_route;
                        } finally {
                            restore_error_handler();
                        }
                    }
                }

                $oas['parameters'][] = $param;
            }
        }

        // security
        $public = $route->getDefault('public');

        if ($public === true) {
            $oas['security'] = [];
        }

        // request body
        if ($request_body = $route->getOption('body')) {
            $oas['requestBody'] = [
                'required' => $request_body['required'] ?? false,
            ];
            $content_types      = $request_body['content-type'] ?? [];
            foreach ($content_types as $content) {
                $oas['requestBody']['content'][$content] = [];
            }
        } elseif ($reflection_method) {
            foreach ($reflection_method->getAttributes(BodyAttribute::class) as $reflection_attribute) {
                $attribute = $reflection_attribute->newInstance();
                $schema    = [
                    'type' => $attribute->getType()->value,
                ];
                if ($attribute->getType() === Type::ARRAY) {
                    $schema['items'] = [];
                }
                /** @var BodyAttribute $attribute */
                $oas['requestBody'] = [
                    'required' => $attribute->isRequired(),
                    'content'  => [
                        $attribute->getContentType()->value => [
                            'schema' => $schema,
                        ],
                    ],
                ];
            }
        }

        // responses
        if ($responses = $route->getOption('responses')) {
            foreach ($responses as $response_code => $response_description) {
                $oas['responses'][$response_code] = [
                    'description' => $response_description,
                ];
            }
        } elseif ($reflection_method) {
            foreach ($reflection_method->getAttributes(ResponseAttribute::class) as $reflection_attribute) {
                /** @var ResponseAttribute $attribute */
                $attribute = $reflection_attribute->newInstance();

                $response_code                    = $attribute->getStatusCode();
                $oas['responses'][$response_code] = [];

                $oas['responses'][$response_code]['description'] = $attribute->getDescription() ?? '';

                if ($attribute->getSchema()) {
                    $schema = [];
                    $ref    = "#/components/schemas/" . $attribute->getSchema();
                    if ($attribute->getType() === Type::ARRAY) {
                        $schema['type']  = Type::ARRAY->value;
                        $schema['items'] = ["\$ref" => $ref];
                    } else {
                        $schema["\$ref"] = $ref;
                    }

                    $content_type = $attribute->getContentType()->value;

                    $oas['responses'][$response_code]['content'][$content_type]['schema'] = $schema;
                }
            }
        }

        // Default response
        if (!isset($oas['responses'])) {
            $oas['responses'] = [
                'default' => [
                    'description' => 'Implement the standard HTTP codes',
                    'content'     => [
                        ContentType::JSON_API->value => [],
                    ],
                ],
            ];
        }

        // add ...
        $path = $route->getPath();

        // ... for each methods
        $http_methods = array_map('strtolower', $route->getMethods());
        foreach ($http_methods as $http_method) {
            $this->documentation['paths'][$path][$http_method] = $oas;
        }
    }


    /**
     * @param mixed $route_type
     *
     * @return string|null
     */
    private function getParametersTypeOAS($route_type): ?string
    {
        return match ($route_type) {
            '\d', '\d+', '\d?', '\d*'            => 'integer',
            '\w', '\w+', '\w?', '\w*', '.*', '*' => 'string',
            '0|1', 'true|false'                  => 'boolean',
            default => null,
        };
    }
}
