<?php
/**
 * @package Mediboard\Core\OpenApi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */


namespace Ox\Components\OASGenerator\Enum;

enum In: string
{
    case HEADER = 'header';
    case PATH = 'path';
    case QUERY = 'query';
    case COOKIE = 'cookie';
}
