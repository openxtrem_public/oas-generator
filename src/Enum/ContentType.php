<?php
/**
 * @package Mediboard\Core\OpenApi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */


namespace Ox\Components\OASGenerator\Enum;

enum ContentType: string
{
    case JSON_API = 'application/vnd.api+json';
    case JSON = 'application/json';
    case HTML = 'text/html';
    case NONE = 'none';
}
