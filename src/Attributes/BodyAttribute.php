<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Attributes;

use Attribute;
use Ox\Components\OASGenerator\Enum\ContentType;
use Ox\Components\OASGenerator\Enum\Type;

/**
 * Describes a single request body.
 */
#[Attribute(Attribute::TARGET_METHOD)]
class BodyAttribute
{

    private ContentType $content_type;
    private bool   $required;
    private Type        $type;

    public function __construct(
        Type $type,
        ContentType $content_type = ContentType::JSON_API,
        bool $required = false,
    ) {
        $this->content_type = $content_type;
        $this->required     = $required;
        $this->type         = $type;
    }

    public function getContentType(): ContentType
    {
        return $this->content_type;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getType(): Type
    {
        return $this->type;
    }

}
