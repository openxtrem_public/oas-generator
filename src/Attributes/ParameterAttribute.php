<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Attributes;

use Attribute;
use Ox\Components\OASGenerator\Enum\Type;
use Ox\Components\OASGenerator\Enum\In;

/**
 * Describes a single operation parameter.
 *
 * A unique parameter is defined by a combination of a name and location.
 */
#[Attribute(Attribute::IS_REPEATABLE | Attribute::TARGET_METHOD)]
class ParameterAttribute
{

    private string  $name;
    private Type    $type;
    private In      $in;
    private bool    $required;
    private ?string $description = null;

    public function __construct(
        string $name,
        Type $type = Type::STRING,
        In $in = In::QUERY,
        bool $required = false,
        string $description = null
    ) {
        $this->name        = $name;
        $this->type        = $type;
        $this->in          = $in;
        $this->required    = $required;
        $this->description = $description;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function getIn(): In
    {
        return $this->in;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

}
