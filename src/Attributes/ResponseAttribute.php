<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Attributes;

use Attribute;
use Ox\Components\OASGenerator\Enum\ContentType;
use Ox\Components\OASGenerator\Enum\Type;

/**
 * Describes a single response.
 */
#[Attribute(Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class ResponseAttribute
{

    private int         $status_code;
    private ContentType $content_type;
    private ?string      $schema;
    private ?Type        $type;
    private ?string      $description;

    public function __construct(
        int $status_code,
        ContentType $content_type = ContentType::JSON_API,
        string $schema = null,
        Type $type = null,
        string $description = null
    ) {
        $this->status_code  = $status_code;
        $this->content_type = $content_type;
        $this->schema       = $schema;
        $this->type         = $type;
        $this->description  = $description;
    }

    public function getStatusCode(): int
    {
        return $this->status_code;
    }

    public function getContentType(): ContentType
    {
        return $this->content_type;
    }

    public function getSchema(): ?string
    {
        return $this->schema;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }
}
