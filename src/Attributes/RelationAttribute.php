<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Attributes;

use Attribute;
use Ox\Components\OASGenerator\Enum\Type;

/**
 * Describes a nested objects relation.
 * An object can include nested object(s)
 */
#[Attribute(Attribute::TARGET_CLASS_CONSTANT)]
class RelationAttribute
{
    private string $schema;
    private Type   $type;
    private bool   $required;

    public function __construct(string $schema, Type $type, bool $required = false)
    {
        $this->schema   = $schema;
        $this->type     = $type;
        $this->required = $required;
    }

    public function getSchema(): string
    {
        return $this->schema;
    }

    public function getType(): Type
    {
        return $this->type;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }
}
