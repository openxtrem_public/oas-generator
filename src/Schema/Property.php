<?php
/**
 * @package Mediboard\Core\OpenApi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Schema;

use Ox\Components\OASGenerator\Enum\Type;

/**
 * For more information about the properties, see JSON Schema Core and JSON Schema Validation.
 * The Schema Object supports keywords from any other vocabularies, or entirely arbitrary properties.
 * @see https://swagger.io/docs/specification/data-models/keywords/
 */
class Property
{
    const FORBIDEN_PROPERTIES = [
        'name',
        'required',
        'custom_fields',
        'ref',
    ];

    private string $name;
    private string $type;
    private bool   $required;

    private ?string $format      = null;
    private ?string $minimum     = null;
    private ?string $maximum     = null;
    private ?string $description = null;
    private mixed   $default     = null;
    private ?string $example     = null;
    private ?string $ref         = null;

    private ?string $pattern          = null;
    private ?int    $exclusiveMinimum = null;
    private ?int    $exclusiveMaximum = null;
    private ?int    $multipleOf       = null;
    private ?int    $minLength        = null;
    private ?int    $maxLength        = null;
    private ?int    $minItems         = null;
    private ?int    $maxItems         = null;
    private ?bool   $uniqueItems      = null;
    private ?int    $minProperties    = null;
    private ?int    $maxProperties    = null;

    private array $enum          = [];
    private array $enum_varnames = [];
    private array $custom_fields = [];

    public function __construct(string $name, string $type, bool $is_required = false)
    {
        $this->name     = $name;
        $this->type     = $type;
        $this->required = $is_required;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function getFormat(): ?string
    {
        return $this->format;
    }

    public function setFormat(?string $format): Property
    {
        $this->format = $format;

        return $this;
    }

    public function getMinimum(): ?string
    {
        return $this->minimum;
    }

    public function setMinimum(?string $minimum): Property
    {
        $this->minimum = $minimum;

        return $this;
    }

    public function getMaximum(): ?string
    {
        return $this->maximum;
    }

    public function setMaximum(?string $maximum): Property
    {
        $this->maximum = $maximum;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Property
    {
        $this->description = $description;

        return $this;
    }

    public function getDefault(): mixed
    {
        return $this->default;
    }

    public function setDefault(mixed $default): Property
    {
        $this->default = $default;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(?string $ref): Property
    {
        $this->ref = $ref;

        return $this;
    }

    public function getPattern(): ?string
    {
        return $this->pattern;
    }

    public function setPattern(?string $pattern): Property
    {
        $this->pattern = $pattern;

        return $this;
    }

    public function getExclusiveMinimum(): ?int
    {
        return $this->exclusiveMinimum;
    }

    public function setExclusiveMinimum(?int $exclusiveMinimum): Property
    {
        $this->exclusiveMinimum = $exclusiveMinimum;

        return $this;
    }

    public function getExclusiveMaximum(): ?int
    {
        return $this->exclusiveMaximum;
    }

    public function setExclusiveMaximum(?int $exclusiveMaximum): Property
    {
        $this->exclusiveMaximum = $exclusiveMaximum;

        return $this;
    }

    public function getMultipleOf(): ?int
    {
        return $this->multipleOf;
    }

    public function setMultipleOf(?int $multipleOf): Property
    {
        $this->multipleOf = $multipleOf;

        return $this;
    }

    public function getMinLength(): ?int
    {
        return $this->minLength;
    }

    public function setMinLength(?int $minLength): Property
    {
        $this->minLength = $minLength;

        return $this;
    }

    public function getMaxLength(): ?int
    {
        return $this->maxLength;
    }

    public function setMaxLength(?int $maxLength): Property
    {
        $this->maxLength = $maxLength;

        return $this;
    }

    public function getMinItems(): ?int
    {
        return $this->minItems;
    }

    public function setMinItems(?int $minItems): Property
    {
        $this->minItems = $minItems;

        return $this;
    }

    public function getMaxItems(): ?int
    {
        return $this->maxItems;
    }

    public function setMaxItems(?int $maxItems): Property
    {
        $this->maxItems = $maxItems;

        return $this;
    }

    public function getUniqueItems(): ?bool
    {
        return $this->uniqueItems;
    }

    public function setUniqueItems(?bool $uniqueItems): Property
    {
        $this->uniqueItems = $uniqueItems;

        return $this;
    }

    public function getMinProperties(): ?int
    {
        return $this->minProperties;
    }

    public function setMinProperties(?int $minProperties): Property
    {
        $this->minProperties = $minProperties;

        return $this;
    }

    public function getMaxProperties(): ?int
    {
        return $this->maxProperties;
    }

    public function setMaxProperties(?int $maxProperties): Property
    {
        $this->maxProperties = $maxProperties;

        return $this;
    }

    public function getEnum(): array
    {
        return $this->enum;
    }

    public function setEnum(array $enum, array $varnames = []): Property
    {
        $this->enum          = $enum;
        $this->enum_varnames = $varnames;

        return $this;
    }

    public function getCustomFields(): array
    {
        return $this->custom_fields;
    }

    public function setCustomFields(array $custom_fields): Property
    {
        $this->custom_fields = $custom_fields;

        return $this;
    }

    public function addCustomFields(string $filed, mixed $value): Property
    {
        $this->custom_fields[$filed] = $value;

        return $this;
    }

    public function getExample(): ?string
    {
        return $this->example;
    }

    public function setExample(?string $example): Property
    {
        $this->example = $example;

        return $this;
    }


    public function toArray(): array
    {
        $data = [];

        foreach (get_object_vars($this) as $var => $value) {
            if (in_array($var, self::FORBIDEN_PROPERTIES)) {
                continue;
            }

            switch (gettype($value)) {
                case 'string':
                case 'integer':
                case 'boolean' :
                    if ($value !== null) {
                        $data[$var] = $value;
                    }
                    break;
                case 'array':
                    if (!empty($value)) {
                        $data[$var] = $value;
                    }
                    break;
            }
        }

        // Nested Objects
        if ($this->ref) {
            if ($this->type === Type::ARRAY->value) {
                $data["items"] = ["\$ref" => $this->ref];
            } else {
                $data["\$ref"] = $this->ref;
            }
        }

        // kustom fields
        if (!empty($this->custom_fields)) {
            $data = array_merge($data, $this->custom_fields);
        }


        return $data;
    }
}
