<?php
/**
 * @package Mediboard\Core\OpenApi
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Schema;

/**
 * The Schema Object allows the definition of input and output data types. These types can be objects, but also
 * primitives and arrays. This object is a superset of the JSON Schema Specification Draft 2020-12.
 */
class Schema
{
    private string  $name;
    private string  $type;
    private string  $ref;
    private ?string $description = null;

    /** @var string[] */
    private array $required = [];

    /** @var Property[] */
    private array $properties = [];

    public function __construct(string $name, string $type = "object")
    {
        $this->name = $name;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getRequired(): array
    {
        return $this->required;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param Property $property
     */
    public function addProperty(Property $property): void
    {
        $this->properties[$property->getName()] = $property->toArray();
        if ($property->isRequired()) {
            $this->required[] = $property->getName();
        }
    }

    public function hasProperties(): bool
    {
        return !empty($this->properties);
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Schema
     */
    public function setDescription(string $description): Schema
    {
        $this->description = $description;

        return $this;
    }

    public function getRef(): string
    {
        return $this->ref;
    }

    public function setRef(string $ref): void
    {
        $this->ref = $ref;
    }

    public function toArray(): array
    {
        return [
            "type"        => $this->type,
            "name"        => $this->name,
            "description" => $this->description,
            "required"    => $this->required,
            "properties"  => $this->properties,
        ];
    }
}
