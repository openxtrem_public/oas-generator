<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Tests;


use Ox\Components\OASGenerator\Schema\Property;
use Ox\Components\OASGenerator\Schema\Schema;
use Ox\Components\OASGenerator\Specifications;
use PHPUnit\Framework\TestCase;

class SpecificationsTest extends TestCase
{
    public function testConvertSpecifications()
    {
        $oas = new Specifications();
        $oas->setVersion('1.2.0')
            ->setTitle('OX APIs documentation')
            ->setDescription('Lorem ipsum dolor set')
            ->setContact('lorem', 'lorem@ipsum.fr')
            ->setLicense('GPL', 'https://openxtrem.com/licenses/gpl.html')
            ->addTag('tag_lorem', 'this is a tag')
            ->addTag('tag_ipsum', 'this is another tag')
            ->addServer('http://lorem-ipsum.fr', 'description')
            ->addSecurity(
                'Token', 'apiKey', 'header token', [
                           'in'   => 'header',
                           'name' => 'X-OXAPI-KEY',
                       ]
            );

        $schema = new Schema("lorem", "object");
        $schema->setDescription('lorem is the better object of the year!');

        $prop1 = new Property('foo', 'string', true);
        $schema->addProperty($prop1);

        $prop2 = new Property('bar', 'integer');
        $prop2->setFormat("int64");
        $prop2->setMinimum(100);
        $prop2->setMaximum(1000);
        $schema->addProperty($prop2);

        $prop3 = new Property('ipsum', 'string', true);
        $prop3->setDescription('ipsum is dolor set');
        $prop3->setEnum(['t1', 't2', 't3'], ['toto', 'tata', 'titi']);
        $schema->addProperty($prop3);

        $oas->addSchema($schema);

        $actual = [
            'openapi'    => '3.0.1',
            'info'       => [
                'title'       => 'OX APIs documentation',
                'description' => 'Lorem ipsum dolor set',
                'version'     => '1.2.0',
                'contact'     => [
                    'name'  => 'lorem',
                    'email' => 'lorem@ipsum.fr',
                ],
                'license'     => [
                    'name' => 'GPL',
                    'url'  => 'https://openxtrem.com/licenses/gpl.html',
                ],
            ],
            'tags'       => [
                [
                    'name'        => 'tag_lorem',
                    'description' => 'this is a tag',
                ],
                [
                    'name'        => 'tag_ipsum',
                    'description' => 'this is another tag',
                ],
            ],
            'servers'    => [
                [
                    'url'         => 'http://lorem-ipsum.fr',
                    'description' => 'description',
                ],
            ],
            'security'   => [
                ['Token' => []],
            ],
            'components' => [
                'securitySchemes' => [
                    'Token' => [
                        'type'        => 'apiKey',
                        'description' => 'header token',
                        'in'          => 'header',
                        'name'        => 'X-OXAPI-KEY',
                    ],
                ],
                'schemas'         => [
                    'lorem' =>
                        [
                            'type'        => 'object',
                            'name'        => 'lorem',
                            'description' => 'lorem is the better object of the year!',
                            'required'    => [
                                'foo',
                                'ipsum',
                            ],
                            'properties'  =>
                                [
                                    'foo'   => [
                                        'type' => 'string',
                                    ],
                                    'bar'   => [
                                        'type'    => 'integer',
                                        'format'  => 'int64',
                                        'minimum' => '100',
                                        'maximum' => '1000',
                                    ],
                                    'ipsum' => [
                                        'type'          => 'string',
                                        'description'   => 'ipsum is dolor set',
                                        'enum'          => ['t1', 't2', 't3'],
                                        'enum_varnames' => ['toto', 'tata', 'titi'],
                                    ],
                                ],
                        ],
                ],
            ],
            'paths'      => [],
        ];

        $this->assertEquals($oas->convert(), $actual);
    }
}