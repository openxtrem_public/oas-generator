<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace Ox\Components\OASGenerator\Tests;

use Exception;
use Ox\Components\OASGenerator\Generator;
use Ox\Components\OASGenerator\Specifications;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Yaml;

class GeneratorTest extends TestCase
{
    private function getSpecifications(): Specifications
    {
        $oas = new Specifications();
        $oas->setVersion('1.0.0')
            ->setTitle('OX APIs documentation')
            ->setDescription('Lorem ipsum dolor set')
            ->setContact('lorem', 'lorem@ipsum.fr')
            ->setLicense('GPL', 'https://openxtrem.com/licenses/gpl.html')
            ->addTag('tag_lorem', 'this is a tag');

        return $oas;
    }

    public function testGenerate()
    {
        $specifications = $this->getSpecifications();

        $collection = new RouteCollection();

        $route = new Route('api/users');
        $route->setMethods(['get', 'post']);
        $collection->add('api_user', $route);

        $route = new Route('api/status');
        $route->setMethods(['get']);
        $collection->add('api_status', $route);

        $generator = new Generator($specifications, $collection);
        $doc       = $generator->generate(false);
        $this->assertCount(2, $doc['paths']);
        $this->assertCount(2, $doc['paths']['/api/users']);
    }

    public function testGenerateWithSpecs()
    {
        $specifications = $this->getSpecifications();

        $collection = new RouteCollection();

        $route = new Route('api/users/{user_id}');
        $route->setMethods(['GET']);
        $route->addRequirements(['sample_movie_id' => '\d+']);
        $route->addDefaults(['permission' => 'read']);
        $route->setOptions([
                               'summary'     => "Summary of the route",
                               'description' => "list all users",
                               'accept'      => ['application/json'],
                               'body'        => [
                                   'required'     => false,
                                   'content-type' => ['application/json'],
                               ],
                               'responses'   => [
                                   200 => 'The response\'s body contains users',
                               ],
                           ]);

        $collection->add('api_user', $route);

        $generator = new Generator($specifications, $collection);
        $doc       = $generator->generate(false);
        $actual    = [
            '/api/users/{user_id}' => [
                'get' => [
                    'summary'     => 'Summary of the route',
                    'description' => 'list all users',
                    'tags'        => ['api'],
                    'parameters'  => [
                        [
                            'in'       => 'path',
                            'name'     => 'sample_movie_id',
                            'schema'   => [
                                'type' => 'integer',
                            ],
                            'required' => true,
                        ],
                    ],
                    'requestBody' => [
                        'required' => false,
                        'content'  => [
                            'application/json' => [],
                        ],
                    ],
                    'responses'   => [
                        '200'       => [
                            'description' => 'The response\'s body contains users',
                        ],
                    ],
                ],
            ],
        ];
        $this->assertEquals($doc['paths'], $actual);
    }

    public function testDoNotGenerate()
    {
        $specifications = $this->getSpecifications();
        $collection     = new RouteCollection();
        $route          = new Route('api/users');
        $route->setMethods(['get', 'post', 'delete']);
        $route->setOption('openapi', false);
        $collection->add('tag_lorem', $route);

        $generator = new Generator($specifications, $collection);
        $doc       = $generator->generate(false);
        $this->assertEmpty($doc['paths']);
    }

    public function testValidateOAS()
    {
        $specifications = $this->getSpecifications();
        $oas            = $specifications->convert();
        unset($oas['info']);

        $stub = $this->createMock(Specifications::class);
        $stub->method('convert')
             ->willReturn($oas);

        $collection = new RouteCollection();
        $route      = new Route('api/users');
        $route->setMethods(['get']);
        $collection->add('api_users', $route);

        $generator = new Generator($stub, $collection);
        $this->expectExceptionMessage('OpenApi is missing required property: info');
        $generator->generate();
    }

    public function testGenerateYaml()
    {
        $specifications = $this->getSpecifications();
        $collection     = new RouteCollection();
        $route          = new Route('api/users');
        $route->setMethods(['get']);
        $collection->add('tag_lorem', $route);

        $generator = new Generator($specifications, $collection);
        $doc_yaml  = $generator->generate(Generator::RETURN_TYPE_YML);
        try {
            $doc_yaml = Yaml::parse($doc_yaml);
            $this->assertNotEmpty($doc_yaml);
        } catch (Exception $e) {
            $this->fail($e->getMessage());
        }
    }
}